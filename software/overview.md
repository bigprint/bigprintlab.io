# Software Overview

This part mainly deals with the software architecture. For information on algorithms, refer to the papers linked in the introduction.

Software can be divided into the three independent components running the whole thing:
- UR5e arms
- Arduino Mega controlling extruder
- Main Computer

Everything is controlled through ROS on the main computer. 
There are multiple libraries used in the planning and control processes.
These are covered in more detail in [Installation](installation.md).

### UR5e arms
In order to receive communication from the main computer via ROS, and external controller must be set up on both UR5e's. 

Instructions on how to do this can be found [here](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver/blob/master/ur_robot_driver/doc/install_urcap_e_series.md).

### Arduino mega
Both arms have an arduino mega used to control the extruder and hotend.

> Need to find out how to set this up.

### Main computer
The main computer is where the planning and contorl of the system happens. 
Installing and running the software required to do this is described in [Installation](installation.md).
