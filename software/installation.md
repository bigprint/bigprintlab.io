# Installation

This page describes how to install and run the planning and control software.

Libraries/Software required for printing with one arm:
- OpenRAVE (installation instructions [here](https://code.research.uts.edu.au/bigprint/2armplanner/tree/master/openrave-installation))
    - simulate robots and perform collision checks
- [Slic3r](https://slic3r.org/)
    - Convert STL models into gcode files
- [gcode2contour](https://code.research.uts.edu.au/bigprint/gcode2contour)
    - convert gcode files to python objects
- [ExtrudeX](https://code.research.uts.edu.au/bigprint/extrudex)
    - TODO: how to use install instructions

Libraries required for printing with 2 arms:
- All libraries for one arm
- [FMTstar](https://code.research.uts.edu.au/bigprint/fmtstar)
- [pyDecMCTS](https://code.research.uts.edu.au/bigprint/pydecmcts)

### Start printing with one arm
- [ ] TODO - calibration procedure.
- [ ] TODO - create a repo for printing with one arm.

By this stage, you should have set up the hardware, installed the software on the Arduino, UR5e and the necessary libraries linked at the top of this page.

Next, you'll want to clone X repo and run Y.
