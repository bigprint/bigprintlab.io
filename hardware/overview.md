# Hardware Overview

Hardware for the project listed below:
- 2x UR5e
- Frame
- Computer
- Extruder Components
    - Mount
    - Extruder + Hotend
    - Arduino
    - RepRap board
- 7x Heating bed

### Electric Connections

> Photo of all the harware conenctions

### Photos
> Multiple photos of full system here documenting how it looks like put together
