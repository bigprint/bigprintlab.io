# Big Print Docs

The Big Print project aims to use multiple robotic arms to 3D print big things faster and stronger.

> Photo of system here

> Maybe a video

#### Motivation
3D printing larger ang larger objects becomes increasingly difficult/expensive with the standard 3-axis gantry printer. 

The use of robotic arms means arbitrarilly large things can be printed.
It also opens the possibility of printing one object with multiple arms, multiplying the printing speed.

Finally, using a 6-DOF robotic arm allows for the printer extruder to approach the print at any angle (vs. only vertically down in a 3-DOF system).
This opens the door to printing layers which are themself 3D, and using this new capability to increase the strength of prints.

> 6 DOF gif here

## Papers

> Link First paper pdf here

## To do list
- [x] Extruder and Sawyer arm working in unison
- [x] 2 arm printing with the sawyer arms
- [ ] Print objects using 1 UR5e printer
- [ ] Build Frame for both arms
- [ ] New Mount design for added rigidity
- [ ] Calibration procedure for both arms (dep: frame built)
- [ ] 2 arm printing with UR5e arms (dep: calibration)
- [ ] Printing 3D contours
- [ ] Generalised algo for 3D infill

---
Jayant Khatkar
